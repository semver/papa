<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/bandung','HomeController@index')->name('bandung');
Route::get('/jakarta', 'HomeController@jakartaIndex')->name('jakarta');
Route::get('/medan', 'HomeController@medanIndex')->name('medan');
Route::get('/palembang', 'HomeController@palembangIndex')->name('palembang');
Route::get('/import', 'HomeController@import');
Route::get('/importHourlyTraffic', 'HomeController@importHourlyTraffic');
Route::get('/importHourlyTrafficUnique', 'HomeController@importHourlyTrafficUnique');
Route::get('/importGraphic', 'HomeController@importGraphic');
Route::get('/importHomeTown', 'HomeController@importHomeTown');
