<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::get('/traffic/{id}', function ($id) {
   return  \App\Traffic::select('total_traffic', 'input_date')->where('location_id',$id)->get();
});

Route::get('/traffic/hourly/{id}', function ($id) {
    return  \App\Traffic::select('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','00_unique','01_unique','02_unique','03_unique','04_unique','05_unique','06_unique','07_unique','08_unique','09_unique','10_unique','11_unique','12_unique','13_unique','14_unique','15_unique','16_unique','17_unique','18_unique','19_unique','20_unique','21_unique','22_unique','23_unique')
                         ->where('location_id',$id)
                        ->where('input_date','2016-06-18 11:32:59')
                        ->get();
});

Route::get('/graphic/{id}', function ($id) {
    return \App\Graphic::where('location_id', $id)->where('input_date', '2016-06-18 00:00:00')->get();
});

Route::get('/hometown/{id}', function ($id) {
   return \App\Hometown::where('location_id',$id)->where('input_date','2016-06-18 00:00:00')->orderBy('total','desc')->take(10)->get();
});
