<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGraphicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('graphics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('location_id');
            $table->timestamp('input_date');
            $table->string('age_17');
            $table->string('age_18_24');
            $table->string('age_25_34');
            $table->string('age_45_54');
            $table->string('age_65');
            $table->string('male');
            $table->string('female');
            $table->string('arpu_50');
            $table->string('arpu_50_100');
            $table->string('arpu_100_200');
            $table->string('arpu_200_500');
            $table->string('arpu_500_1000');
            $table->string('culinary');
            $table->string('shopping');
            $table->string('music');
            $table->string('outdoor');
            $table->string('sport');
            $table->string('fashion');
            $table->string('as');
            $table->string('lp');
            $table->string('sp');
            $table->string('kh');
            $table->string('a');
            $table->string('b');
            $table->string('c');
            $table->string('d');
            $table->string('e');
            $table->string('data_y');
            $table->string('data_n');
            $table->string('smarthphone');
            $table->string('featurephone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('graphics');
    }
}
