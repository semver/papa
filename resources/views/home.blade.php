@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-lg-6 col-xs-12 col-sm-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase font-dark">Total Traffic</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default" href="#">
                            <i class="icon-cloud-upload"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="#">
                            <i class="icon-wrench"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="#">
                            <i class="icon-trash"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title=""
                           title=""> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <canvas id="myChart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-xs-12 col-sm-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase font-dark">Total Unique</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default" href="#">
                            <i class="icon-cloud-upload"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="#">
                            <i class="icon-wrench"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="#">
                            <i class="icon-trash"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-title=""
                           title=""> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <canvas class="myChart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        {{--Traffic each Day--}}
        <div class="col-lg-6 col-xs-12 col-sm-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase font-dark">Traffic per Date</span>
                        <span class="caption-helper">distance stats...</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <canvas id="myLineChart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xs-12 col-sm-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase font-dark">Traffic per Hour 18 June 2016</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <canvas id="myHourChart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase font-dark">Graphics per 18 June 2016</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-4">
                            <h2 style="text-align: center">Age</h2>
                            <canvas id="myAgePie" width="100" height="100"></canvas>
                        </div>
                        <div class="col-md-4">
                            <h2 style="text-align: center">Gender</h2>
                            <canvas id="myPieGender" width="100" height="100"></canvas>
                        </div>
                        <div class="col-md-4">
                            <h2 style="text-align: center">Interest</h2>
                            <canvas id="myPieInterest" width="100" height="100"></canvas>
                        </div>
                        <div class="col-md-4">
                            <h2 style="text-align: center">Mobile Spend</h2>
                            <canvas id="myMobileSpend" width="100" height="100"></canvas>
                        </div>
                        <div class="col-md-4">
                            <h2 style="text-align: center">Device Type</h2>
                            <canvas id="myDeviceType" width="100" height="100"></canvas>
                        </div>
                        <div class="col-md-4">
                            <h2 style="text-align: center">Card Brand</h2>
                            <canvas id="myCardBrand" width="100" height="100"></canvas>
                        </div>
                        <div class="col-md-4">
                            <h2 style="text-align: center">SES</h2>
                            <canvas id="mySES" width="100" height="100"></canvas>
                        </div>
                        <div class="col-md-4">
                            <h2 style="text-align: center">Data User</h2>
                            <canvas id="myDataUser" width="100" height="100"></canvas>
                        </div>
                        <div class="col-md-4">
                            <h2 style="text-align: center">Top Hometown</h2>
                            <canvas id="myTopHomeTown" width="100" height="100"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer')

    <script>
        var ctx = document.getElementById("myChart");
        var ctx_line = document.getElementById("myLineChart");
        var ctx_hour = document.getElementById("myHourChart");
        var ctx_age_pie = document.getElementById("myAgePie");
        var ctx_gender_pie = document.getElementById("myPieGender");
        var ctx_interest_pie = document.getElementById("myPieInterest");

        var data = {
            labels: ['Traffic', 'Avg Weekday', 'Avg Weekend'],
            datasets: [{
                label: "My First dataset",
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                ],
                borderColor: [
                    'rgba(255,99,132,1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                ],
                borderWidth: 1,
                data: [
                    {{ $totalAll }},
                    {{ $weekday }},
                    {{ $weekend }}
                ]
            }]
        }
        var myBarChart = new Chart(ctx, {
            type: 'bar',
            data: data
        });

        var lineLabel = [];
        var lineData = [];
        $.getJSON('api/traffic/' + '{{ $location }}', function (data) {
            $.each(data, function (key, val) {
                lineLabel.push([moment(val.input_date).format('DD-MMM').toString()]);
                lineData.push(val.total_traffic);
            })
            var data_line = {
                labels: lineLabel,
                datasets: [
                    {
                        label: "Traffic Each Date",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: lineData,
                        spanGaps: true,
                    }
                ]
            };
            var myLineChart = new Chart(ctx_line, {
                type: 'line',
                data: data_line,
                options: {
                    scales: {
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
            });
        }).fail(function (response) {
            window.console.error(response)
        });


        var lineLabelHourly = [
            '00:00',
            '01:00',
            '02:00',
            '03:00',
            '04:00',
            '05:00',
            '06:00',
            '07:00',
            '08:00',
            '09:00',
            '10:00',
            '11:00',
            '12:00',
            '13:00',
            '14:00',
            '15:00',
            '16:00',
            '17:00',
            '18:00',
            '19:00',
            '20:00',
            '21:00',
            '22:00',
            '23:00'
        ];
        var lineDataHourly = [];
        var linDataHourlyUnique = [];
        $.getJSON('api/traffic/hourly/' + '{{ $location }}', function (data) {
            lineDataHourly = [
                data['0']['00'],
                data['0']['01'],
                data['0']['02'],
                data['0']['03'],
                data['0']['04'],
                data['0']['05'],
                data['0']['06'],
                data['0']['07'],
                data['0']['08'],
                data['0']['09'],
                data['0']['10'],
                data['0']['11'],
                data['0']['12'],
                data['0']['13'],
                data['0']['14'],
                data['0']['15'],
                data['0']['16'],
                data['0']['17'],
                data['0']['18'],
                data['0']['19'],
                data['0']['20'],
                data['0']['21'],
                data['0']['22'],
                data['0']['23']
            ];
            linDataHourlyUnique = [
                data['0']['00_unique'],
                data['0']['01_unique'],
                data['0']['02_unique'],
                data['0']['03_unique'],
                data['0']['04_unique'],
                data['0']['05_unique'],
                data['0']['06_unique'],
                data['0']['07_unique'],
                data['0']['08_unique'],
                data['0']['09_unique'],
                data['0']['10_unique'],
                data['0']['11_unique'],
                data['0']['12_unique'],
                data['0']['13_unique'],
                data['0']['14_unique'],
                data['0']['15_unique'],
                data['0']['16_unique'],
                data['0']['17_unique'],
                data['0']['18_unique'],
                data['0']['19_unique'],
                data['0']['20_unique'],
                data['0']['21_unique'],
                data['0']['22_unique'],
                data['0']['23_unique']
            ];
            var data_line_hourly = {
                labels: lineLabelHourly,
                datasets: [
                    {
                        label: "Traffic Per Time Hour Unique",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: linDataHourlyUnique,
                        spanGaps: true,
                    },
                    {
                        label: "Traffic Per Time Hour",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(239, 112, 121,0.4)",
                        borderColor: "rgba(239, 112, 121,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(239, 112, 121,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(239, 112, 121,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: lineDataHourly,
                        spanGaps: true,
                    }

                ]
            };
            var myLineChartHourly = new Chart(ctx_hour, {
                type: 'line',
                data: data_line_hourly,
                options: {
                    scales: {
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
            });
        }).fail(function (response) {
            window.console.error(response)
        });

        var ageData = [];
        var genderData = [];
        $.getJSON('api/graphic/' + '{{ $location }}', function (data) {
            window.console.log(data)
            ageData = [
                data[0]['age_17'],
                data[0]['age_18_24'],
                data[0]['age_25_34'],
                data[0]['age_35_44'],
                data[0]['age_45_54'],
                data[0]['age_55_64'],
                data[0]['age_65']
            ];
            genderData = [
               data[0]['male'],
               data[0]['female']
            ];
            var dataAgePie = {
                labels: [
                    "17",
                    "18_24",
                    "25_34",
                    '35_44',
                    '45_54',
                    '55_64',
                    '65'
                ],
                datasets: [
                    {
                        data: ageData,
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56",
                            '#A0E4F1',
                            '#ECFFC9',
                            '#2A769A',
                            '#FFD6A6'
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56",
                            '#A0E4F1',
                            '#ECFFC9',
                            '#2A769A',
                            '#FFD6A6'
                        ]
                    }]
            };
            var dataGenderPie = {
                labels: [
                  'male','female'
                ],
                datasets: [
                    {
                        data: genderData,
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                        ]
                    }]
            };
            new Chart(ctx_age_pie, {
                type: 'pie',
                data: dataAgePie
            });
            new Chart(ctx_gender_pie,{
                type: 'pie',
                data: dataGenderPie
            });
            var interestData = [
                    data[0]['culinary'],
                    data[0]['fashion'],
                    data[0]['sport'],
                    data[0]['outdoor'],
                    data[0]['music'],
                    data[0]['shopping']
            ];
            window.console.log(interestData)
            var dataInterest = {
                labels: [
                    'culinary',
                    'fashion',
                    'sport',
                    'outdoor',
                    'music',
                    'shopping'
                ],
                datasets: [
                    {
                        data: interestData,
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56",
                            '#A0E4F1',
                            '#ECFFC9',
                            '#2A769A'
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56",
                            '#A0E4F1',
                            '#ECFFC9',
                            '#2A769A'
                        ]
                    }]
            };
             new Chart($('#myPieInterest'),{
                type: 'pie',
                data: dataInterest
            })
            var dataMobileSpend = {
                labels: [
                    'arpu_50K',
                    'arpu_50K_100K',
                    'arpu_100K_200K',
                    'arpu_200K_500K',
                    'arpu_500K_1000K',
                    'arpu_1000K'
                ],
                datasets: [
                    {
                        data: [
                           data[0]['arpu_50'],
                           data[0]['arpu_50_100'],
                           data[0]['arpu_100_200'],
                           data[0]['arpu_200_500'],
                           data[0]['arpu_500_1000'],
                           data[0]['arpu_1000']
                        ],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56",
                            '#A0E4F1',
                            '#ECFFC9',
                            '#2A769A'
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56",
                            '#A0E4F1',
                            '#ECFFC9',
                            '#2A769A'
                        ]
                    }]
            };
            new Chart($('#myMobileSpend'),{
                type: 'pie',
                data: dataMobileSpend
            });
            var dataDevice = {
                labels: [
                    'smartphone',
                    'featurephone'
                ],
                datasets: [
                    {
                        data: [
                            data[0]['smartphone'],
                            data[0]['featurephone']
                        ],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB"
                        ]
                    }]
            };
            new Chart($('#myDeviceType'),{
                type: 'pie',
                data: dataDevice
            });
            var dataCard = {
                labels: [
                    'as',
                    'lp',
                    'sp',
                    'kh'
                ],
                datasets: [
                    {
                        data: [
                            data[0]['as'],
                            data[0]['lp'],
                            data[0]['sp'],
                            data[0]['kh']
                        ],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56",
                            '#A0E4F1'
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56",
                            '#A0E4F1'
                        ]
                    }]
            };
            new Chart($('#myCardBrand'),{
                type: 'pie',
                data: dataCard
            });
            var dataSES = {
                labels: [
                    'a',
                    'b',
                    'c',
                    'd',
                    'e'
                ],
                datasets: [
                    {
                        data: [
                            data[0]['a'],
                            data[0]['b'],
                            data[0]['c'],
                            data[0]['d'],
                            data[0]['e']
                        ],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56",
                            '#A0E4F1',
                            '#82A6EE'
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56",
                            '#A0E4F1',
                            '#82A6EE'
                        ]
                    }]
            };
            new Chart($('#mySES'),{
                type: 'pie',
                data: dataSES
            });
            var dataUser = {
                labels: [
                    'y','n'
                ],
                datasets: [
                    {
                        data: [
                            data[0]['data_y'],
                            data[0]['data_n']
                        ],
                        backgroundColor: [
                            '#A0E4F1',
                            '#82A6EE'
                        ],
                        hoverBackgroundColor: [
                            '#A0E4F1',
                            '#82A6EE'
                        ]
                    }]
            };
            new Chart($('#myDataUser'),{
                type: 'pie',
                data: dataUser
            })
        }).fail(function (response) {
            window.console.error(response)
        });
        var hometownLabel = [];
        var homeTownData = [];
        $.getJSON('api/hometown/' + '{{$location}}', function (data) {
            $.each(data, function (key, value) {
                hometownLabel.push(value.origin);
                homeTownData.push(value.total);
            })
            var hometownDataChart = {
                labels: hometownLabel,
                datasets: [{
                    label: "Best top 10 HomeTown",
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56",
                        '#A0E4F1',
                        '#ECFFC9',
                        '#2A769A',
                        '#FFD6A6'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56",
                        '#A0E4F1',
                        '#ECFFC9',
                        '#2A769A',
                        '#FFD6A6'
                    ],
                    borderWidth: 1,
                    data: homeTownData
                }]
            };
            new Chart($('#myTopHomeTown'), {
                type: 'horizontalBar',
                data: hometownDataChart
            })
        }).fail( function (response) {
            window.console.error(response)
        })


    </script>
@stop
