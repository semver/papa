<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Graphic extends Model
{
    protected $fillable = ['location_id',
        'input_date',
        'age_17',
        'age_18_24',
        'age_25_34' ,
        'age_35_44',
        'age_45_54',
        'age_55_64' ,
        'age_65',
        'male',
        'female',
        'arpu_50',
        'arpu_50_100',
        'arpu_100_200',
        'arpu_200_500',
        'arpu_500_1000',
        'arpu_1000',
        'culinary',
        'fashion',
        'sport',
        'outdoor',
        'music',
        'shopping',
        'as',
        'lp',
        'sp',
        'kh',
        'a',
        'b',
        'c',
        'd',
        'e',
        'data_y',
        'data_n',
        'smartphone',
        'featurephone'
    ];
}
