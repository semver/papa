<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Traffic extends Model
{
    protected $fillable = [
        'location_id',
        'type',
        'total_traffic',
        'input_date'
    ];
}
