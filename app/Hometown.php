<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hometown extends Model
{
    protected $fillable = ['location_id','origin','total','input_date'];
}
