<?php

namespace App\Http\Controllers;

use App\Graphic;
use App\Hometown;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Traffic;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalAll = Traffic::where('location_id', '2')->sum('total_traffic');
        $weekday = Traffic::where('location_id', '2')->where('type', 'weekday')->avg('total_traffic');
        $weekend = Traffic::where('location_id', '2')->where('type', 'weekend')->avg('total_traffic');
        $trafficEachDay = Traffic::select('total_traffic', 'input_date')->where('location_id', '2')->get();
        $bandungActive = 'active';
        $location = 2;
        return view('home', compact('totalAll', 'weekday', 'weekend', 'bandungActive', 'trafficEachDay', 'location'));
    }

    public function jakartaIndex()
    {
        $totalAll = Traffic::where('location_id', '1')->sum('total_traffic');
        $weekday = Traffic::where('location_id', '1')->where('type', 'weekday')->avg('total_traffic');
        $weekend = Traffic::where('location_id', '1')->where('type', 'weekend')->avg('total_traffic');
        $jakartaActive = 'active';
        $location = 1;
        return view('home', compact('totalAll', 'weekday', 'weekend', 'jakartaActive', 'location'));
    }

    public function medanIndex()
    {
        $totalAll = Traffic::where('location_id', '4')->sum('total_traffic');
        $weekday = Traffic::where('location_id', '4')->where('type', 'weekday')->avg('total_traffic');
        $weekend = Traffic::where('location_id', '4')->where('type', 'weekend')->avg('total_traffic');
        $medanActive = 'active';
        $location = 4;
        return view('home', compact('totalAll', 'weekday', 'weekend', 'medanActive','location'));
    }

    public function palembangIndex()
    {
        $totalAll = Traffic::where('location_id', '3')->sum('total_traffic');
        $weekday = Traffic::where('location_id', '3')->where('type', 'weekday')->avg('total_traffic');
        $weekend = Traffic::where('location_id', '3')->where('type', 'weekend')->avg('total_traffic');
        $palembangActive = 'active';
        $location = 3;
        return view('home', compact('totalAll', 'weekday', 'weekend', 'palembangActive','location'));
    }

    public function importHourlyTraffic()
    {
        $a = Excel::load(storage_path('traffic_hourly.xlsx'))->get();
//        dd($a[24]);
        //        foreach ($a as $excel ) {
//            if (strpos($excel->location, 'Jakarta') !== false) {
//                $excel->location = 2;
//            } if (strpos($excel->location, 'Bandung') !== false) {
//                $excel->location = 1;
//            } if (strpos($excel->location, 'Medan') !== false) {
//                $excel->location = 3;
//            } if (strpos($excel->location, 'Pelembang') !== false) {
//                $excel->location = 4;
//            }
//            Traffic::where('location_id',$excel->location)
//                    ->where('input_date', $excel->date)
//                    ->update([
//                       '0' => $excel['0'],
//                       '1' => $excel['1'],
//                       '2' => $excel['2'],
//                       '3' => $excel['3'],
//                       '4' => $excel['4'],
//                       '5' => $excel['5'],
//                       '6' => $excel['6'],
//                       '7' => $excel['7'],
//                       '8' => $excel['8'],
//                       '9' => $excel['9'],
//                       '10' => $excel['10'],
//                       '11' => $excel['11'],
//                       '12' => $excel['12'],
//                       '13' => $excel['13'],
//                       '14' => $excel['14'],
//                       '15' => $excel['15'],
//                       '16' => $excel['16'],
//                       '17' => $excel['17'],
//                       '18' => $excel['18'],
//                       '19' => $excel['19'],
//                       '20' => $excel['20'],
//                       '21' => $excel['21'],
//                       '22' => $excel['22'],
//                       '23' => $excel['23'],
//                    ]);
//        }
        $collection = collect([130, 112, 84, 71, 166, 183, 135, 156, 334, 352, 388, 430, 450, 476, 482, 460, 456, 409, 325, 295, 282, 275, 210, 184]);
        Traffic::where('id', '>=', 24)
            ->update([
                '0' => $collection->random(),
                '1' => $collection->random(),
                '2' => $collection->random(),
                '3' => $collection->random(),
                '4' => $collection->random(),
                '5' => $collection->random(),
                '6' => $collection->random(),
                '7' => $collection->random(),
                '8' => $collection->random(),
                '9' => $collection->random(),
                '10' => $collection->random(),
                '11' => $collection->random(),
                '12' => $collection->random(),
                '13' => $collection->random(),
                '14' => $collection->random(),
                '15' => $collection->random(),
                '16' => $collection->random(),
                '17' => $collection->random(),
                '18' => $collection->random(),
                '19' => $collection->random(),
                '20' => $collection->random(),
                '21' => $collection->random(),
                '22' => $collection->random(),
                '23' => $collection->random(),
            ]);
    }

    public function import()
    {
        $a = Excel::load(storage_path('Workbook1.xlsx'))->get();
//        dd($a);
        foreach ($a as $excel) {
            if (strpos($excel->location, 'Jakarta') !== false) {
                $excel->location = 2;
            }
            if (strpos($excel->location, 'Bandung') !== false) {
                $excel->location = 1;
            }
            if (strpos($excel->location, 'Medan') !== false) {
                $excel->location = 3;
            }
            if (strpos($excel->location, 'Pelembang') !== false) {
                $excel->location = 4;
            }
            Traffic::create([
                'location_id' => $excel->location,
                'type' => 'weekday',
                'total_traffic' => $excel->total,
                'input_date' => $excel->date
            ]);
        }
        echo 'bissmilah';
    }

    public function importHomeTown()
    {
        $a = Excel::load(storage_path('hometown.xlsx'))->get();
//        dd($a);
        foreach ($a as $excel){
            if (strpos($excel->location, 'Jakarta') !== false) {
                $excel->location = 2;
            }
            if (strpos($excel->location, 'Bandung') !== false) {
                $excel->location = 1;
            }
            if (strpos($excel->location, 'Medan') !== false) {
                $excel->location = 3;
            }
            if (strpos($excel->location, 'Pelembang') !== false) {
                $excel->location = 4;
            }
            Hometown::create([
                'location_id' => $excel->location,
                'origin' => $excel->origin,
                'total' => $excel->total,
                'input_date' => $excel->date
            ]);
        }

    }
    public function importGraphic()
    {
        $a = Excel::load(storage_path('graphics.xlsx'))->get();
//        dd($a);
        foreach ($a as $excel) {
            if (strpos($excel->location, 'Jakarta') !== false) {
                $excel->location = 2;
            }
            if (strpos($excel->location, 'Bandung') !== false) {
                $excel->location = 1;
            }
            if (strpos($excel->location, 'Medan') !== false) {
                $excel->location = 3;
            }
            if (strpos($excel->location, 'Pelembang') !== false) {
                $excel->location = 4;
            }
            Graphic::create([
                'location_id' => $excel->location,
                'input_date' => $excel->date,
                'age_17' => $excel->age_17,
                'age_18_24' => $excel->age_18_24,
                'age_25_34' => $excel->age_25_34,
                'age_35_44' => $excel->age_35_44,
                'age_45_54' => $excel->age_45_54,
                'age_55_64' => $excel->age_55_64,
                'age_65' => $excel->age_65,
                'male' => $excel->male,
                'female' => $excel->female,
                'arpu_50' => $excel->arpu_50k,
                'arpu_50_100' => $excel->arpu_50k,
                'arpu_100_200' => $excel->arpu_50_100k,
                'arpu_200_500' => $excel->arpu_200_500k,
                'arpu_500_1000' => $excel->arpu_500k_1000k,
                'arpu_1000' => $excel->arpu_1000k,
                'culinary' => $excel->culinary,
                'fashion' => $excel->fashion,
                'sport' => $excel->sport,
                'outdoor' => $excel->outdoor_activity,
                'music' => $excel->music,
                'shopping' => $excel->shopping,
                'as' => $excel->as,
                'lp' => $excel->lp,
                'sp' => $excel->sp,
                'kh' => $excel->kh,
                'a' => $excel->a,
                'b' => $excel->b,
                'c' => $excel->c,
                'd' => $excel->d,
                'e' => $excel->e,
                'data_y' => $excel->y,
                'data_n' => $excel->n,
                'smartphone' => $excel->smartphone,
                'featurephone' => $excel->featurephone,
            ]);
        }
    }

}
